to set up the board:

Grab all categories. Choose 6 that are exactly 5 clues each. 
Assign values based on their x index. 

When a user clicks the cell pop up a question board for them to answer. 
When the user types in an answer the value is added to their score if they got it right, and does nothing if they got it wrong
Once a question is answered make the tile unclickable and remove the value so its a blank square. 
