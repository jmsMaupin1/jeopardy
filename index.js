const BASE_URL = 'https://jservice.xyz';
let questionBoard = document.getElementById('question-board');
let questionContent = document.getElementById('question-content');
let loadingScreen = document.getElementById('loading-screen');
let answer = document.getElementById('answer');

let score = 0;
let question = {};

function randBetween(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getAllCategories(cb) {
    fetch(`${BASE_URL}/api/categories`)
    .then(res => res.json())
    .then(json => cb(json))
}

function getClueByID(id) {
    return fetch(`${BASE_URL}/api/clues?category=${id}`)
        .then(res => res.json());
}

function checkAnswer(answer) {
    let cell = document.getElementById(question.id);
    let scoreText = document.querySelector('#score > h1')
    cell.onclick = () => false;
    cell.textContent = '';

    if (answer === question.answer) {
        score += question.value;
        scoreText.textContent = `Score: ${score}`;
    }
}

function createClueCell(clue) {
    let cell = document.createElement('div')
    cell.classList.add('cell');
    cell.textContent = clue.value
    cell.id = clue.id;
    cell.onclick = () => {
        document.getElementById('question').textContent = clue.question;
        question = {
            question: clue.question,
            answer: clue.answer,
            id: clue.id,
            value: clue.value
        }
        console.log(question)
        questionBoard.style.display = "flex";
    }
    answer.value = '';
    document.getElementById('jboard').appendChild(cell);
}

function createCategoryHeader(category) {
    let cell = document.createElement('div');
    cell.classList.add('cell');
    cell.textContent = category;

    document.getElementById('jboard').appendChild(cell);
}

function prepareData(clues) {
    for(let clue in clues) {
        if(clues[clue].length !== 5) 
            delete clues[clue]
    }

    let keys = Object.keys(clues);

    for(let i = 0; i < 6; ++i) {
        createCategoryHeader(keys[i])
    }

    for(let x = 0; x < 5; x++) {
        for(let y = 0; y < 6; y++) {
            let { answer, question, id } = clues[keys[y]][x];
            createClueCell({
                answer,
                question,
                id,
                value: (200*(x + 1))
            })
        }
    }
    
    loadingScreen.style.display = "none";
}

getAllCategories(json => {
    let numOfCategories = json.categories.length;
    let categories = [];
    let clues = [];

    for(let i = 0; i < 16; i++) {
        let rand = Math.floor(Math.random() * numOfCategories);
        let category = json.categories[rand];

        if(categories.indexOf(category) === -1)
            categories.push(category);
        else i--;
    }

    for (let category of categories) {
        clues.push(getClueByID(category.id))
    }

    Promise.all(clues).then(clue => {
        prepareData(clue.reduce( (pre, cur) => {
            let title = cur.clues[0].category.title;
            pre[title] = cur.clues;
            return pre;
        }, {}))
    }).catch(e => {
        console.log(e)
    })
})

questionContent.onclick = e => {
    e.stopPropagation();
}

questionBoard.onclick = e => {
    questionBoard.style.display = "none";
}

answer.addEventListener('keypress', function(e) {
    if (e.keyCode !== 13) return;

    checkAnswer(answer.value);
    questionBoard.style.display = "none";
})